﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SpawnPointManager : MonoBehaviour {
    private Queue<Transform> m_points = new Queue<Transform>();

    private void Awake()
    {
        var points = transform.GetComponentsInChildren<Transform>();
        foreach(var p in points)
        {
            m_points.Enqueue(p);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Transform ConsumeSpawnPoint()
    {
        if (m_points.Count == 0)
            return null;
        else
            return m_points.Dequeue();
    }
}
