﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine;
using System.Collections;
using UnityEditor;
using AI;

[CustomEditor(typeof(EnemyUnit))]
public class EnemyCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("STUN"))
        {
            ((EnemyUnit)target).Stun();
        }
    }
}
