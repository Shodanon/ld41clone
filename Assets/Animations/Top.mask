%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Top
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/Armature_001
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone/Bone_001
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone/Bone_002
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone/Bone_003
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone/Bone_004
    m_Weight: 0
  - m_Path: Armature/Armature_001/Bone/Bone_005
    m_Weight: 0
  - m_Path: Armature/Armature_002
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone/Bone_012
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone/Hammer
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone/Lever
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone/Reciver
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone/Trigger
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009/Bone_010
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009/Bone_010/Shell_005
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009/Bone_010/Shell_005/Bullet_005
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009/Shell_004
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Bone_009/Shell_004/Bullet_004
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Shell_003
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Bone_008/Shell_003/Bullet_003
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Shell_002
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Bone_007/Shell_002/Bullet_002
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Shell_001
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Bone_006/Shell_001/Bullet_001
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Shell
    m_Weight: 0
  - m_Path: Armature/Armature_002/BaseBone_001/Bone_6/Shell/Bullet
    m_Weight: 0
  - m_Path: Armature/Base
    m_Weight: 1
  - m_Path: Armature/Base/Foot_L
    m_Weight: 0
  - m_Path: Armature/Base/Foot_L/Toes_L
    m_Weight: 0
  - m_Path: Armature/Base/Foot_R
    m_Weight: 0
  - m_Path: Armature/Base/Foot_R/Toes_R
    m_Weight: 0
  - m_Path: Armature/Base/IKLegPullTraget_L
    m_Weight: 1
  - m_Path: Armature/Base/IKLegPullTraget_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Head
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Head/Hair
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Index_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Index_L/Index_L_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Index_L/Index_L_002/Index_L_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Pinky_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Pinky_L/Pinky_L_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Pinky_L/Pinky_L_002/Pinky_L_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/RingFinger_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/RingFinger_L/RingFinger_L_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/RingFinger_L/RingFinger_L_002/RingFinger_L_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Thumb1_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/Thumb1_L/Thumb2_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/TrafficFinger_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/TrafficFinger_L/TrafficFinger_L_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_L/TrafficFinger_L/TrafficFinger_L_002/TrafficFinger_L_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Index_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Index_R/Index_R_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Index_R/Index_R_002/Index_R_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Pinky_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Pinky_R/Pinky_R_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Pinky_R/Pinky_R_002/Pinky_R_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/RingFinger_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/RingFinger_R/RingFinger_R_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/RingFinger_R/RingFinger_R_002/RingFinger_R_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Thumb1_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/Thumb1_R/Thumb2_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/TrafficFinger_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/TrafficFinger_R/TrafficFinger_R_002
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/2HIKHandel/Hand_R/TrafficFinger_R/TrafficFinger_R_002/TrafficFinger_R_001
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/HandIKPullTarget_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/IKHandBase/HandIKPullTarget_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Neck
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_L/UpperArm_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_L/UpperArm_L/LowerArm_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_R/UpperArm_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Sholder_R/UpperArm_R/LowerArm_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Tit_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/Tit_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/TitHandle_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Chest/TitHandle_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/HIps
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/HIps/LegUpper_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/HIps/LegUpper_L/LegLower_L
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/HIps/LegUpper_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/HIps/LegUpper_R/LegLower_R
    m_Weight: 1
  - m_Path: Armature/Base/WaistBase/Waist
    m_Weight: 1
  - m_Path: BanditoMask
    m_Weight: 1
  - m_Path: Cube
    m_Weight: 1
  - m_Path: Cube_001
    m_Weight: 1
  - m_Path: Cube_002
    m_Weight: 1
  - m_Path: Hat_Hair
    m_Weight: 1
  - m_Path: SherrifStar
    m_Weight: 1
  - m_Path: StandardOutfit
    m_Weight: 1
