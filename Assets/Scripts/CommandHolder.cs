﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class CommandHolder : MonoBehaviour
    {
        private List<Command> m_active_command = new List<Command>();
        private List<Queue<Command>> m_command_queue = new List<Queue<Command>>();
        private List<Queue<Command>> m_completed_commands = new List<Queue<Command>>();
        private Vector3 m_start_position;
        private bool m_restart = false;

        public const int MAX_CHANNELS = 3;


        void Awake()
        {
            m_start_position = gameObject.transform.position;
            for(int i = 0; i < MAX_CHANNELS; i++)
            {
                m_active_command.Add(null);
                m_command_queue.Add(new Queue<Command>());
                m_completed_commands.Add(new Queue<Command>());
            }
        }

        private void Start()
        {
            for (int i = 0; i < MAX_CHANNELS; i++)
            {
                StartCoroutine(Think(i));
            }
        }



        private IEnumerator Think(int commandPath)
        {
            while (true)
            {
                yield return ConsumeCommands(commandPath);
                yield return new WaitForEndOfFrame();
            }
        }

        
        private IEnumerator ConsumeCommands(int commandPath = 0)
        {
            while(m_command_queue[commandPath].Count > 0 || false) {
                var command = m_command_queue[commandPath].Dequeue();
                if (command is DynamicCommand)
                {
                    yield return command.Fire(gameObject);
                    command = ((DynamicCommand)command).FreezeState();
                }
                m_completed_commands[commandPath].Enqueue(command);
                m_active_command[commandPath] = command;
                yield return command.Fire(gameObject);
                m_active_command[commandPath] = null;
            }

            yield return null;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //RequestRestart();
            }

            for (int i = 0; i < MAX_CHANNELS; i++)
            {
                if (m_active_command[i] != null)
                m_active_command[i].NotifyTimePassed(Time.deltaTime * Time.timeScale);
            }

        }

        public bool StreamInUse(int commandPath)
        {
            return (m_active_command[commandPath] != null);
        }

        // clears the queue, cancels the current command, and starts a new path
        public void Interrupt(int commandPath)
        {
            m_command_queue[commandPath].Clear();

            if (m_active_command[commandPath] != null)
            m_active_command[commandPath].Cancel();
        }

        // Adds the command to the end of its queue
        //e
        public void Queue(Command c, int commandPath = 0)
        {
            m_command_queue[commandPath].Enqueue(c);
        }

        /*public void InterruptThenQueueCommand(Command c, int commandPath = 0)
        {
            Interrupt();
            Queue(c);
        }
        */

        // pretend we executed all of our old commands
        // Then set all commands we've done as our new command queue
        public void RequestRestart()
        {
            // We're not "canceling" our active command, it should still continue
            
            for(int i = 0; i < MAX_CHANNELS; i++) {
                if (m_active_command[i] != null)
                {
                    m_active_command[i].Restarting();
                    m_completed_commands[i].Enqueue(m_active_command[i]);
                }

                StopAllCoroutines();
            }

            for (int i = 0; i < MAX_CHANNELS; i++) {
                foreach (var command in m_command_queue[i])
                {
                    m_completed_commands[i].Enqueue(command);
                }
            }

            m_command_queue = m_completed_commands;
            for (int i = 0; i < MAX_CHANNELS; i++)
            {
                m_completed_commands[i] = new Queue<Command>();
            }

            gameObject.GetComponent<NavMeshAgent>().Warp(m_start_position);

            for (int i = 0; i < MAX_CHANNELS; i++)
            {
                StartCoroutine(Think(i));
            }
        }
    }
}