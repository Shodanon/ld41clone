﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.Audio;

public class TimeControl : MonoBehaviour
{
    public static Stack<PlayerLimbo> limboCharacters = new Stack<PlayerLimbo>();
	public static AudioMixer masterMixer;

    private const float MIN_TIMESCALE = 0.04F;
    private const float LOOK_SCALE = 0.7F;
    private const float LERP_SPEED = 10F;
    private const float LOOP_TIME = 30.0F;
    public const float RECORDING_TICK = 0.004f;

    private static float m_timeScale;
    private static float m_tempTimeScale = 0;
    private static float m_tempTimeScaleDuration;
    private static float m_TimeRemaining;
    private static bool m_TimePaused = false;

    private static float playerTimeScale = 1;

    private RigidbodyFirstPersonController m_player;

    // Use this for initialization
    void Start()
    {
		masterMixer = Resources.Load("MainAudio") as AudioMixer;
        m_player = FindObjectOfType<RigidbodyFirstPersonController>();
        m_TimeRemaining = LOOP_TIME;
    }

    void Update()
    {
        // TODO: change time scaling based on game state (paused, etc.) ,


        if (m_player != null)
            //playerTimeScale = m_player.timeScaleLook > m_player.timeScaleMove ? m_player.timeScaleLook : m_player.timeScaleMove;

            // Lerp the timescale to the tempTimeScale, unless we are getting close to the end of the duration, then lerp back to the normal timescale
        m_tempTimeScale = m_tempTimeScaleDuration > 0.1 ? m_tempTimeScale : playerTimeScale;
        float lerpTimeScale = m_tempTimeScale;
        if (playerTimeScale < m_tempTimeScale)
        {
            lerpTimeScale = Mathf.Lerp(m_timeScale, m_tempTimeScale, Time.deltaTime * LERP_SPEED);

        }
        else
        {
            lerpTimeScale = Mathf.Lerp(m_timeScale, playerTimeScale, Time.deltaTime * LERP_SPEED);

        }
        
        m_timeScale = lerpTimeScale;


        // ensure timeScale is greater than MIN_TIMESCALE
        m_timeScale = m_timeScale > MIN_TIMESCALE ? m_timeScale : MIN_TIMESCALE;
        if(!m_TimePaused)
        {
            // clamp timeScale to 1.0
            Time.timeScale = m_timeScale < 1.0F ? m_timeScale : 1.0F;
            Time.fixedDeltaTime = 0.02F * Time.timeScale;
        }
        else
        {
            Time.timeScale = 0;
        }

        if (m_tempTimeScaleDuration > 0)
        {
            m_tempTimeScaleDuration -= Time.deltaTime;
        }

		masterMixer.SetFloat ("Pitch", m_timeScale);
		//Debug.Log ("ts: " + Time.timeScale);
    }

    private void FixedUpdate()
    {
        if (!m_TimePaused)
        {
            m_TimeRemaining -= Time.fixedDeltaTime;
        }
    }

    public static float GetTimeRemaining()
    {
        return m_TimeRemaining;
    }

    public static bool GetPaused()
    {
        return m_TimePaused;
    }

    public static void SetPaused(bool paused)
    {
        m_TimePaused = paused;
        if (m_TimePaused)
        {
            m_timeScale = 0;
        }
        else
        {

        }
    }

    public static void ResetTime()
    {
        m_TimeRemaining = LOOP_TIME;
    }
    public static void SetTimeRemaining(float TimeRemaining)
    {
        m_TimeRemaining = TimeRemaining;
    }

    public static void SetTimeScaleForDuration(float newTimeScale, float duration)
    {
        m_tempTimeScale = newTimeScale;
        m_tempTimeScaleDuration = duration + 0.1F; // add time to lerp
    }

    public static void TimeSlow(float scale)
    {
        playerTimeScale = scale;
    }
}