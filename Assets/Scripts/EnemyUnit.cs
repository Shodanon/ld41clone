﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AI;
using UnityEngine.AI;
using UnityStandardAssets.Characters.FirstPerson;


namespace AI
{
	[RequireComponent (typeof(CommandHolder))]
	[RequireComponent (typeof(NavMeshAgent))]
	[RequireComponent (typeof(MeshCollider))]
	[RequireComponent (typeof(CapsuleCollider))]
	public class EnemyUnit : MonoBehaviour
	{
		[SerializeField]
		private float DIST_NEW_MOVE = 2.0F;
		[SerializeField]
		private float DIST_TO_SHOOT = 5.0F;
		[SerializeField]
		private float DIST_TO_TEST_SIGHT = 25.0f;

		[SerializeField]
		private RigidbodyFirstPersonController m_player;
		[SerializeField]
		private float speed = 5.0F;

		[SerializeField]
		private float m_health = 100F;

		private Vector3 m_lastPlayerPos;
		private CommandHolder m_command;
		public float m_fireTime = 0.0F;

		public enum STATE
		{
IDLE,
			CHASE,
			SHOOT,
			STUNNED,
			DEAD}

		;


		[SerializeField]
		private STATE m_state = STATE.IDLE;

		public NavMeshAgent m_agent;
		[SerializeField]
		public GameObject m_weapon;

		public Animator CurrentWeapon;
		float swingSpeed;

		// Use this for initialization
		void Start ()
		{
			m_fireTime -= Time.fixedDeltaTime;

			if (m_weapon != null)
				m_weapon.GetComponent<WeaponProperties> ().SetOwner (this.gameObject);

			m_agent = GetComponent<NavMeshAgent> ();
			m_command = GetComponent<CommandHolder> ();

			if (m_player == null) {
				m_player = FindObjectOfType<RigidbodyFirstPersonController> ();
			}

			/*
            m_command.Queue(new DynamicCommand());
            m_command.Queue(new CreatePathCommand(new Vector3(-5, 0, 0), 5f));
            m_command.Queue(new Log("First Path is done"));
            m_command.Queue(new CreatePathCommand(new Vector3(0, 0, 0), 5f));
            m_command.Queue(new DynamicCommand());
            m_command.Queue(new Log("Both paths finished"));
			*/

			m_command.Queue (new Move (gameObject.transform.position, speed));

			//NavMeshHit hit;
			//if (NavMesh.SamplePosition (m_player.transform.position, out hit, 12F, NavMesh.AllAreas)) {
			//	m_lastPlayerPos = hit.position;
			//	m_command.Queue (new Move (m_lastPlayerPos, speed));
			//}
		}

		// Update is called once per frame
		void Update ()
		{
			Vector3 playerPos = m_player.cam.transform.position;
			Vector3 pos = gameObject.transform.position + new Vector3 (0, 2, 0);
			float distToPlayer = Vector3.Distance (playerPos, pos);
			switch (m_state) {
			case STATE.IDLE:
				//Debug.Log ("IDLE");
				if (distToPlayer < DIST_TO_TEST_SIGHT) {
					// close enough to see player
					float dot = Vector3.Dot (transform.forward, (playerPos - pos).normalized);
					//Debug.Log (dot);
					if (dot > 0) {
						// looking towards player
						if (CanSeePlayer (pos, playerPos)) {
							if (Vector3.Distance (playerPos, pos) < DIST_TO_SHOOT)
								m_state = STATE.SHOOT;
							else
								m_state = STATE.CHASE;
							break;
						}
					}
				}
				break;
			case STATE.CHASE:
				//Debug.Log ("CHASE");
				if (distToPlayer < DIST_TO_SHOOT) {
					if (CanSeePlayer (pos, playerPos)) {
						m_command.Interrupt (0);
						m_command.Queue (new Stop ());
						m_state = STATE.SHOOT;
						break;
					}
				} else if (Vector3.Distance (m_lastPlayerPos, m_player.transform.position) > DIST_NEW_MOVE) {
					NavMeshHit hit;
					if (NavMesh.SamplePosition (m_player.transform.position, out hit, 12F, NavMesh.AllAreas)) {
						//Debug.Log ("New move");
						m_lastPlayerPos = hit.position;
						m_command.Queue (new Move (m_lastPlayerPos, speed));
					}
				}
				break;
			case STATE.SHOOT:
				Debug.Log ("SHOOT");
				Quaternion targetRotation = Quaternion.LookRotation (playerPos - pos);
				transform.rotation = Quaternion.Lerp (transform.rotation, targetRotation, Mathf.Min (1, 0.1F));
				if (!m_command.StreamInUse (1)) { // Check if attack stream is free
					Debug.Log ("m_fireTime: " + m_fireTime);
					if (Quaternion.Angle (transform.rotation, targetRotation) < 10 && m_fireTime <= 0) {
						m_fireTime = 1.5F;
						m_command.Queue (new DynamicAttack (m_player.gameObject), 1);
					}
				} else {
					if (distToPlayer > DIST_TO_SHOOT) {
						m_state = STATE.CHASE;
						break;
					} else if (!CanSeePlayer (pos, playerPos)) {
						m_state = STATE.CHASE;
						break;
					}
				}

				break;			
			case STATE.STUNNED:
				Debug.Log ("STUN");
				break;
			case STATE.DEAD:
				Debug.Log ("DEAD");
				break;
			}
			m_fireTime -= Time.fixedDeltaTime;
		}

		public void Stun ()
		{
			m_command.Interrupt (0);
			m_command.Interrupt (1);
			m_command.Interrupt (2);
			m_state = STATE.STUNNED;
			m_command.Queue (new Stun ());
		}

		// switches to the state if we're not doing anything
		public void RequestState (STATE target)
		{
			if (m_state == STATE.IDLE || m_state == STATE.STUNNED) {
				m_state = target;
			}
		}

		public void Damage (float amount)
		{
			// TODO: add stun
			m_health -= amount;
			if (m_health <= 0)
				Die ();
		}

		public void Die ()
		{
			m_state = STATE.DEAD;
			GetComponent<MeshRenderer> ().enabled = false;
			GetComponent<MeshCollider> ().enabled = false;
			GetComponent<CapsuleCollider> ().enabled = false;
			// reset weapon
			if (m_weapon != null)
				m_weapon.GetComponent<WeaponProperties> ().ClearOwner ();
		}

		public bool CanSeePlayer (Vector3 pos, Vector3 playerPos)
		{
			RaycastHit hit;
			if (Physics.Raycast (pos, (playerPos - pos).normalized, out hit, DIST_TO_TEST_SIGHT)) {
				if (hit.rigidbody && hit.rigidbody.gameObject.Equals (m_player.gameObject)) {
					// player is seen
					return true;
				}
			}
			return false;
		}
	}
}

class DynamicAttack : DynamicCommand
{
	private GameObject m_victim;

	public DynamicAttack (GameObject victim)
	{
		m_victim = victim;
	}

	public override Command FreezeState ()
	{
		return new FireAtPosition (m_victim.transform.position);
	}
}

class Stun : Command
{
	public override IEnumerator Process ()
	{
		var enemy = m_target.GetComponent<EnemyUnit> ();
		// TODO: theoretically drop weapon  

		m_target.GetComponent<EnemyUnit> ().RequestState (EnemyUnit.STATE.IDLE);
		yield return new WaitForSeconds (1);
	}
}

class FireAtPosition : Command
{
	private Vector3 m_pos;

	public FireAtPosition (Vector3 pos)
	{
		m_pos = pos;
	}
	// This blocks only attack actions until it exits
	public override IEnumerator Process ()
	{
		WeaponProperties wep = m_target.GetComponentInChildren<WeaponProperties> ();
		//if (wep)
		//	wep.Shoot ();
		if (wep != null) {
			Animator CurrentWeapon = wep.GetComponent<Animator> ();
			if (CurrentWeapon != null) {
				if (CurrentWeapon.GetCurrentAnimatorStateInfo (0).IsTag ("Idle") && CurrentWeapon.transform.GetComponent<WeaponProperties> ().Ammocount > 0) {
					CurrentWeapon.SetTrigger ("SHOOT");
					if (CurrentWeapon.transform.GetComponent<WeaponProperties> ().Ammocount == 0)
						m_target.GetComponent<EnemyUnit> ().m_fireTime = 0;
				} else if (CurrentWeapon.GetCurrentAnimatorStateInfo (0).IsTag ("Reload") || CurrentWeapon.transform.GetComponent<WeaponProperties> ().ReloadAnyTime) {
					CurrentWeapon.SetTrigger ("reload");
					CurrentWeapon.SetFloat ("reloadSpeed", 1);
				}
			}
		}
		Debug.Log ("AI is actually shooting");
		yield return base.Process ();// new WaitForSeconds(0.2f); // Replace 2f with reload time
	}
}