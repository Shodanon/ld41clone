﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AnimatorSounds : MonoBehaviour {
    AudioSource AudioSrc;
    Bob bobbingScript;
    // Use this for initialization
    void Start () {
        AudioSrc = gameObject.AddComponent<AudioSource>();
		AudioSrc.transform.parent = gameObject.GetComponent<WeaponProperties> ().transform;
		string group = "SFX";
		AudioMixer masterMixer = Resources.Load("MainAudio") as AudioMixer;
		AudioMixerGroup audioGroup = masterMixer.FindMatchingGroups (group) [0];
		AudioSrc.outputAudioMixerGroup = audioGroup;
        AudioSrc.volume = 0.6f;
        bobbingScript = GameObject.Find("WeaponSlot").GetComponent<Bob>();
    }

    // Update is called once per frame
    public void PlaySound (AudioClip ToPlay) {
        AudioSrc.PlayOneShot(ToPlay);
        if (ToPlay.name.Contains("blast"))
        {
            bobbingScript.shoot();
        }
    }
}
