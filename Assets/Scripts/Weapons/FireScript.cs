﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class FireScript : MonoBehaviour
{
    public Animator CurrentWeapon;
    private Bob lookBob;
    private ParticleSystem Smoke;
    float swingSpeed;

    private RigidbodyFirstPersonController controller;
    // Use this for initialization
    void Start()
    {
        lookBob = transform.GetComponent<Bob>();
        //Smoke = CurrentWeapon.transform.GetComponentInChildren<ParticleSystem>();
        controller = transform.parent.parent.GetComponent<RigidbodyFirstPersonController>();
    }

    // Update is called once per frame



    void Update()
    {

        if (CurrentWeapon != null)
        {
            swingSpeed -= Input.GetAxis("Mouse X");
            swingSpeed = Mathf.Clamp(swingSpeed, -1f, 1f);
            swingSpeed = Mathf.Lerp(swingSpeed, 0, 2f * Time.smoothDeltaTime);

            CurrentWeapon.SetFloat("speed", swingSpeed);
            if (Input.GetButtonDown("Fire1"))
            {
                if (CurrentWeapon.GetCurrentAnimatorStateInfo(0).IsTag("Idle") && CurrentWeapon.transform.GetComponent<WeaponProperties>().Ammocount > 0)
                {

                    if (Smoke != null)
                    {
                        Smoke.Emit(100);
                    }
                    CurrentWeapon.SetTrigger("SHOOT");

                }
            }
            if (Input.GetButtonDown("Reload"))
            {
                if (CurrentWeapon.GetCurrentAnimatorStateInfo(0).IsTag("Reload") || CurrentWeapon.transform.GetComponent<WeaponProperties>().ReloadAnyTime)
                {

                    TimeControl.SetTimeScaleForDuration(0.4f, 3);
                    CurrentWeapon.SetTrigger("reload");
                    CurrentWeapon.SetFloat("reloadSpeed", 1);

                }
            }
        }
    }

    public void SetCurrentWeapon(Transform weapon)
    {
        CurrentWeapon = weapon.GetComponent<Animator>();
    }
    public void removeWeapon()
    {
        CurrentWeapon = null;
    }
}
