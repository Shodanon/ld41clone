﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;
using AI;

public class WeaponProperties : MonoBehaviour {
    public int numberOfProjectiles;
    public float bulletSpeed;
    private RigidbodyFirstPersonController controller;
    private CommandHolder m_command;
    public GameObject bulletPrefab;
    public Camera cam;
    public Vector3 WeaponPosition;
    public int MaxAmmocount;
    public int Ammocount;
    public bool ReloadAnyTime;
	public GameObject m_owner;
	[SerializeField]
	private float m_damage;
	private float m_repeatSpeed = 0.25F;
	private float m_repeatTime = 0.0F;

    // Use this for initialization
    void Start () {
        controller = GameObject.Find ("RigidBodyFPSController").GetComponent < RigidbodyFirstPersonController>();
        m_command = controller.GetComponent<CommandHolder>();
        cam = Camera.main;
        Ammocount = MaxAmmocount;
    }

	public void SetOwner(GameObject owner)
	{
		m_owner = owner;
		GetComponent<BoxCollider> ().enabled = false;
		GetComponent<Rigidbody> ().isKinematic = true;
		transform.position = owner.transform.position + new Vector3(0,1,0);
		transform.rotation = new Quaternion();
		transform.parent = owner.transform;
	}

	public void ClearOwner()
	{
		m_owner.transform.DetachChildren ();
		GetComponent<BoxCollider> ().enabled = true;
		GetComponent<Rigidbody> ().isKinematic = false;
		m_owner = null;
	}

    // Update is called once per frame
    public void Shoot () {
		if (!m_owner) // player is shooting
	        controller.Fire();
		if (Ammocount > 0)
			FireWeapon (bulletSpeed, numberOfProjectiles);
		else
			ReloadAmmo ();
    }

    void FireWeapon(float projectileSpeed, int projectileNumber)
    {
        //Debug.Log("fired");
        Ammocount -= 1;
		if (!m_owner) // player is shooting
	        TimeControl.SetTimeScaleForDuration(1.0F, 0.5F);

		//if (m_owner)
		//	gameObject.GetComponent<Animator> ().SetTrigger ("SHOOT");

        List<GameObject> exceptions = new List<GameObject>();
		if (m_owner)
			exceptions.Add (m_owner.gameObject);
		else
	        exceptions.Add(controller.gameObject);
        exceptions.Add(gameObject);

        if (m_command != null)
        {
            for (int a = 0; a < projectileNumber; ++a)
            {
				Vector3 spawnPosition = cam.transform.position + cam.transform.forward;
				Quaternion spawnRotation = cam.transform.rotation;
				if (m_owner != null && m_owner.GetComponent<EnemyUnit> ()) {
					spawnPosition = transform.position;
					spawnRotation = transform.rotation;
				}
				GameObject spawnedBulletPrefab = GameObject.Instantiate (bulletPrefab, spawnPosition, spawnRotation);
				Vector3 randomSpray = Vector3.up * UnityEngine.Random.Range (-0.1f, 0.1f) + Vector3.right * UnityEngine.Random.Range (-0.1f, 0.1f);
				if (a == 0) {
					randomSpray = Vector3.zero;
				}
				m_command.Queue (
					new SpawnProjectile (spawnPosition,
						(spawnRotation * (Vector3.forward + randomSpray)) * projectileSpeed,
						m_damage,
						exceptions,
						spawnedBulletPrefab));
				exceptions.Add (spawnedBulletPrefab);
            }
        }


    }

    public void ReloadAmmo()
	{
		//if (m_owner)
		//	gameObject.GetComponent<Animator> ().SetTrigger ("reload");
        Ammocount = MaxAmmocount;
    }

}
