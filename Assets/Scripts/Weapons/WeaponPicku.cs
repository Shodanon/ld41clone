﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponPicku : MonoBehaviour {
    private Ray DetectWeapon;
    private RaycastHit hitto;
    private Bob bobDestination;
    private Transform PickedUpWeapon;
    private WeaponProperties  weaponProps;
    private FireScript  fireController;
    private Text WeaponName;
    private Text AmmoCount;

    // Use this for initialization
    void Start () {
        bobDestination = GetComponentInChildren<Bob>();
        fireController= GetComponentInChildren<FireScript>();
        WeaponName = GameObject.Find("Ammo").GetComponent<Text>();
        AmmoCount = GameObject.Find("AmmoCount").GetComponent<Text>();

    }

    // Update is called once per frame
    void Update () {

        DetectWeapon = new Ray(transform.position, transform.forward);

        if( Physics.Raycast(DetectWeapon,  out hitto, 2.5f) && PickedUpWeapon==null)
        {
            if (hitto.collider.GetComponent<WeaponProperties>())
            {
				if (hitto.collider.GetComponent<WeaponProperties> ().m_owner == null) {
					if (Input.GetButton ("Pickup")) {
						PickedUpWeapon = hitto.collider.transform;
						weaponProps = PickedUpWeapon.GetComponent<WeaponProperties> ();
						PickedUpWeapon.parent = bobDestination.childLerped;
						PickedUpWeapon.GetComponent<Rigidbody> ().isKinematic = true;
						PickedUpWeapon.GetComponent<Animator> ().SetFloat ("reloadSpeed", 1);
						fireController.SetCurrentWeapon (PickedUpWeapon);
					}
				}
            }
        }
        if (PickedUpWeapon != null)
        {
            if (PickedUpWeapon.localPosition != Vector3.zero + weaponProps.WeaponPosition)
            {
                PickedUpWeapon.localPosition = Vector3.Lerp(PickedUpWeapon.localPosition, Vector3.zero + weaponProps.WeaponPosition, 5f * Time.deltaTime);
            }
            if (PickedUpWeapon.localRotation != Quaternion.Euler(Vector3.zero))
            {
                PickedUpWeapon.localRotation = Quaternion.Slerp(PickedUpWeapon.localRotation, Quaternion.Euler(Vector3.zero), 5f * Time.deltaTime);
            }
            WeaponName.text = PickedUpWeapon.name;
            AmmoCount.text = PickedUpWeapon.GetComponent<WeaponProperties>().Ammocount+"/"+ PickedUpWeapon.GetComponent<WeaponProperties>().MaxAmmocount;
            if (Input.GetButton("Throw"))
            {
                PickedUpWeapon.parent = null;
                PickedUpWeapon.GetComponent<Rigidbody>().isKinematic = false;
                PickedUpWeapon.GetComponent<Rigidbody>().AddForce(transform.forward*15f,ForceMode.Impulse);
                PickedUpWeapon.GetComponent<Rigidbody>().AddTorque(Random.insideUnitSphere*15f, ForceMode.Impulse);
                PickedUpWeapon.GetComponent<Animator>().SetFloat("reloadSpeed", 0);
                fireController.removeWeapon();

                weaponProps = null;
                PickedUpWeapon = null;
            }
        }
        else
        {
            WeaponName.text = "";
            AmmoCount.text = "";
        }
        Debug.DrawRay(transform.position, DetectWeapon.direction);

    }
}
