﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bob : MonoBehaviour
{
    public Transform childLerped;
    private Vector3 LimitedPosition;
    private Quaternion LimitedRotation;
    private Rigidbody parentRigibody;
    private Vector3 vel = Vector3.zero;

    public Vector3 ShootMod;

    // Use this for initialization
    void Start()
    {
        childLerped = transform.Find("Lerped");
        if (childLerped != false)
            return;
        childLerped.parent = null;
        LimitedPosition = transform.position;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ShootMod.magnitude > 0.01f)
        {
            ShootMod = Vector3.Lerp(ShootMod, Vector3.zero, 10f * Time.fixedDeltaTime);
        }
        LimitedPosition = Vector3.SmoothDamp(LimitedPosition, transform.position, ref vel, 0.2f * Time.fixedDeltaTime) + ShootMod;
        LimitedRotation = Quaternion.Slerp(LimitedRotation, transform.rotation, 20 * Time.fixedDeltaTime);
    }

    private void Update()
    {
        if (Time.timeScale < 0.9f)
        {
            LimitedPosition = Vector3.Lerp(LimitedPosition, transform.position, Time.timeScale * Time.smoothDeltaTime);

        }
        LimitedRotation = Quaternion.Slerp(LimitedRotation, transform.rotation, Time.timeScale * Time.smoothDeltaTime);

        if (childLerped != false)
            return;
        childLerped.rotation = LimitedRotation;
        childLerped.position = LimitedPosition;

    }
    
    public void shoot()
    {
        ShootMod = transform .rotation * Vector3.back * 0.3f;
    }
}