﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpSound : MonoBehaviour {
    AudioSource AudioSrc;

    public AudioClip[] JumpSounds;

    public AudioClip Fall;
    private bool fall;
    // Use this for initialization
    void Start () {
        if (gameObject.GetComponent<AudioSource>() == null)
        {
            AudioSrc = gameObject.AddComponent<AudioSource>();
        }
        else
        {
            AudioSrc = gameObject.GetComponent<AudioSource>();
        }

        AudioSrc.playOnAwake = false;
        AudioSrc.volume = 0.6f;

    }

    private void Update()
    {
        if (transform.position.y < -5&&!fall)
        {
            AudioSrc.PlayOneShot(Fall);
            fall = true;
        }
    }

    // Update is called once per frame
    public void PlayJump()
    {
        if (!AudioSrc.isPlaying)
        {
            AudioSrc.PlayOneShot(JumpSounds[Random.Range(0, JumpSounds.Length)]);

        }
    }
}
