﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
    public AudioSource[] MusicLayers;
    private float fade;

    private float speedMusic;
    
    // Use this for initialization
    void Start () {
        speedMusic = Time.timeScale;

        MusicLayers[0].volume = 0.5f;
        MusicLayers[1].volume = 0;
        MusicLayers[2].volume = 0;
        speedMusic=0f;
    }

    // Update is called once per frame
    void Update () {


        speedMusic = Mathf.Lerp(speedMusic, Time.timeScale, Time.unscaledDeltaTime);

        MusicLayers[0].volume = 0.5f - speedMusic/2f;
        MusicLayers[1].volume = speedMusic / 2f;

    }


}
