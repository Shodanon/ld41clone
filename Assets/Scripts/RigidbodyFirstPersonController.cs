using System;
using System.Collections;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using AI;
using System.Collections.Generic;

namespace UnityStandardAssets.Characters.FirstPerson
{
    public struct recordCommand
    {
        public float timeRemaining;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 velocity;
        public float crouchHeight;
        public bool fire;
    }

    [RequireComponent(typeof (Rigidbody))]
    [RequireComponent(typeof (CapsuleCollider))]
	[RequireComponent(typeof (CommandHolder))]
    public class RigidbodyFirstPersonController : MonoBehaviour
    {
        // a value between 1 and 0
        public float timeScaleLook = 0;
        public float timeScaleMove = 0;

        public float timeRemaining = 30.0f;
        public int m_MaxHealth = 100;
        public SpawnPoint CurrentSpawnPoint;
        public SpawnPoint NextSpawnPoint;
        public PlayerReplay ReplayCharacter;
        public PlayerLimbo LimboCharacter;

        [Serializable]
        public class MovementSettings
        {
            public float Speed = 10.0f;   // Speed when walking forward
            [Range(1.0f, 10.0f)] public float Acceleration = 1.0f;
            [Range(1.0f, 10.0f)] public float Deceleration = 1.0f;
            public float JumpForce = 30f;
            public float CrouchHeight = 0.8f;
            public float CameraCrouchHeight = 0.2f;
            public AnimationCurve SlopeCurveModifier = new AnimationCurve(new Keyframe(-90.0f, 1.0f), new Keyframe(0.0f, 1.0f), new Keyframe(90.0f, 0.0f));
            [HideInInspector] public float CurrentTargetSpeed = 10f;

            public void UpdateDesiredTargetSpeed(Vector2 input)
            {
	            if (input == Vector2.zero) return;
				if (input.x > 0 || input.x < 0)
				{
					//strafe
					CurrentTargetSpeed = Speed;
				}
				if (input.y < 0)
				{
					//backwards
					CurrentTargetSpeed = Speed;
				}
				if (input.y > 0)
				{
					//forwards
					//handled last as if strafing and moving forward at the same time forwards speed should take precedence
					CurrentTargetSpeed = Speed;
				}
            }

        }


        [Serializable]
        public class AdvancedSettings
        {
            public float groundCheckDistance = 0.01f; // distance for checking if the controller is grounded ( 0.01f seems to work best for this )
            public float stickToGroundHelperDistance = 0.5f; // stops the character
            public float slowDownRate = 20f; // rate at which the controller comes to a stop when there is no input
            public bool airControl; // can the user control the direction that is being moved in the air
            public bool VerticalMoveTimeScale;
            [Tooltip("set it to 0.1 or more if you get stuck in wall")]
            public float shellOffset; //reduce the radius by that ratio to avoid getting stuck in wall (a value of 0.1f is nice)
        }


        public Camera cam;
        public MovementSettings movementSettings = new MovementSettings();
        public MouseLook mouseLook = new MouseLook();
        public AdvancedSettings advancedSettings = new AdvancedSettings();
        enum MovementMode { MM_None, MM_Walking, MM_Falling }
		[SerializeField]
		public GameObject bulletPrefab;
        //TODO Create stack of in-limbo characters

        private Rigidbody m_RigidBody;
		private CapsuleCollider m_Capsule;
        private float m_RotationChange;
        private float m_StopTime;
        private Vector3 m_GroundContactNormal;
        private Vector3 m_HorizontalSpeed = new Vector3(0, 0, 0);
        private Vector3 m_SmoothingVelocity = Vector3.zero;
        private Quaternion m_CameraRotation = new Quaternion(0, 0, 0, 1);
        private bool m_Jump, m_PreviouslyGrounded, m_Jumping, m_IsGrounded, m_Crouched, m_Crouching;

        /****************Do Not Edit*****************/
        Vector3 d_PrevVel = new Vector3(0, 0, 0);
        Vector3 d_InputDirection = new Vector3(0, 0, 0);
        Vector3 d_ImpulseVector = new Vector3(0, 0, 0);
        Vector3 d_InputSideVector = new Vector3(0, 0, 0);
        Vector3 d_InputForwardVector = new Vector3(0, 0, 0);
        Vector3 d_CharacterMove = new Vector3(0, 0, 0);
        float d_XZSpeedometer = 0.0f;
        float d_ProjectedVelocity = 0.0f;
        float d_InputSpeed = 0.0f;
        float d_AccelSpeedCap = 8.0f;
        float d_MaxAccelSpeed = 0.0f;
        float d_AccelSpeed = 5.0f;
        float d_DefaultFriction = 0.0f;
        float d_DefaultMaxWalkSpeed = 0.0f;
        float d_DefaultBraking = 0.0f;
        float d_FrameTime = 0.0f;
        float d_YVelocity = 0.0f;
        float d_InputSideAxis = 0.0f;
        float d_InputForwardAxis = 0.0f;
        float d_StandingHeight = 1.8f;
        float d_CameraStandingHeight = 6.0f;
        float d_StartCrouchHeight = 1.8f;
        float d_StartPosition = 0.0f;
        float d_GoalHeight = 1.8f;
        float d_CameraGoalHeight = 0.6f;
        float d_StartCameraHeight = 0.6f;
        bool d_InAir = false;
        bool d_IsRampsliding = false;
		private bool m_Firing = false;

        /****************Air Acceleration*****************/
        float airAccelerate = 10.0f;

        /****************Ground Acceleration*****************/
        float groundAccelerate = 10.0f;

        /****************Rampslide*****************/
        float rampslideThresholdFactor = 2.5f;
        float rampMomentumFactor = 2.5f;

        float CrouchTime = 0.0f;

        public int m_Health;
        private bool m_Dead;

        /*****************Recording*****************/
        private const float RECORDING_TICK = TimeControl.RECORDING_TICK;
        private float m_TimeSinceLastTick = 0.0f;
        private bool m_Recording = true;
        public List<recordCommand> m_RecordingLog;

        public Vector3 Velocity
        {
            get { return m_RigidBody.velocity; }
        }

        public bool Grounded
        {
            get { return m_IsGrounded; }
        }

        public bool Jumping
        {
            get { return m_Jumping; }
        }

        public bool Running
        {
            get
            {
	            return false;
            }
        }

        // Friction
        private void ResetFriction()
        {
            movementSettings.Speed = d_DefaultMaxWalkSpeed;
            m_RigidBody.drag = d_DefaultFriction;
            movementSettings.Deceleration = d_DefaultBraking;
        }
        private void RemoveFriction()
        {
            movementSettings.Deceleration = 0.0f;
            m_RigidBody.drag = 0.0f;
        }

        // Movement
        private void MoveForward(float AxisValue)
        {
            d_InputForwardAxis = AxisValue;
            d_InputForwardVector.x = cam.transform.forward.x;
            d_InputForwardVector.z = cam.transform.forward.z;
            d_InputForwardVector.y = 0.0f;
            d_InputForwardVector = d_InputForwardVector.normalized;
            Move();
        }
        private void MoveRight(float AxisValue)
        {
            d_InputSideAxis = AxisValue;
            d_InputSideVector.x = cam.transform.right.x;
            d_InputSideVector.z = cam.transform.right.z;
            d_InputSideVector.y = 0.0f;
            d_InputSideVector = d_InputSideVector.normalized;
            Move();
        }
        private void Move()
        {
            if (d_InAir)
            {
                d_IsRampsliding = false;
                AccelerateAir(d_InputForwardVector, d_InputForwardAxis, d_InputSideVector, d_InputSideAxis);
            }
            else
            {
                if (RampCheck() && d_XZSpeedometer > d_DefaultMaxWalkSpeed * rampslideThresholdFactor)
                {
                    RemoveFriction();
                    d_IsRampsliding = true;
                    AccelerateAir(d_InputForwardVector, d_InputForwardAxis, d_InputSideVector, d_InputSideAxis);
                }
                else
                {
                    if (d_IsRampsliding)
                    {
                        m_RigidBody.AddForce(new Vector3(0.0f, 0.0f, d_YVelocity * rampMomentumFactor), ForceMode.Impulse);
                        d_IsRampsliding = false;
                        StartCoroutine(GroundMoveDelay(d_FrameTime * 2));
                    }
                    else
                    {
                        GroundMove();
                    }
                }
            }
        }
        private void GroundMove()
        {
            AddMovementInput(d_InputForwardVector * d_InputForwardAxis + d_InputSideVector * d_InputSideAxis);
            if (!d_InAir)
            {
                AccelerateGround(d_InputForwardVector, d_InputForwardAxis, d_InputSideVector, d_InputSideAxis);
            }
        }
        private bool RampCheck()
        {
            GroundCheck();
            float dot = Vector3.Dot(m_GroundContactNormal.normalized, d_PrevVel.normalized);
            return Mathf.Acos(dot) * Mathf.Rad2Deg > 92.0f;
        }
        private void AccelerateAir(Vector3 ForwardVector, float ForwardAxis, Vector3 SideVector, float SideAxis)
        {
            d_InputDirection = (ForwardVector * ForwardAxis + SideVector * SideAxis).normalized;
            d_ProjectedVelocity = Vector3.Dot(d_PrevVel, d_InputDirection);
            d_InputSpeed = (d_InputDirection * d_DefaultMaxWalkSpeed).magnitude;
            d_AccelSpeedCap = 80.0f;
            d_MaxAccelSpeed = Mathf.Clamp(d_InputSpeed, 0.0f, d_AccelSpeedCap) - d_ProjectedVelocity;
            if(!(d_MaxAccelSpeed <= 0.0f))
            {
                d_AccelSpeed = Mathf.Clamp(d_FrameTime * d_InputSpeed * airAccelerate, 0.0f, d_MaxAccelSpeed);
                d_ImpulseVector = d_InputDirection * d_AccelSpeed;
                movementSettings.Speed = Mathf.Clamp((d_PrevVel + d_ImpulseVector).magnitude, d_DefaultMaxWalkSpeed, 10000);
                //m_RigidBody.AddForce(d_ImpulseVector, ForceMode.Impulse);
                AddMovementInput(d_ImpulseVector);
            }
        }
        private void AccelerateGround(Vector3 ForwardVector, float ForwardAxis, Vector3 SideVector, float SideAxis)
        {
            d_InputDirection = (ForwardVector * ForwardAxis + SideVector * SideAxis).normalized;
            if (d_XZSpeedometer >= d_DefaultMaxWalkSpeed * 0.9)
            {
                d_ProjectedVelocity = Vector3.Dot(d_PrevVel, d_InputDirection);
                d_InputSpeed = (d_InputDirection * d_DefaultMaxWalkSpeed).magnitude;
                d_MaxAccelSpeed = d_InputSpeed - d_ProjectedVelocity;
                if (d_MaxAccelSpeed <= 0.0f)
                {
                    movementSettings.Speed = d_DefaultMaxWalkSpeed;
                }
                else
                {
                    d_AccelSpeed = Mathf.Clamp(d_FrameTime * d_InputSpeed * groundAccelerate, 0.0f, d_MaxAccelSpeed);
                    d_ImpulseVector = d_InputDirection * d_AccelSpeed;
                    movementSettings.Speed = Mathf.Clamp((d_PrevVel + d_ImpulseVector).magnitude, d_DefaultMaxWalkSpeed, 10000);
                    m_RigidBody.AddForce(d_ImpulseVector, ForceMode.Impulse);
                }
            }
            else
            {
                movementSettings.Speed = d_DefaultMaxWalkSpeed;
            }
        }
        private void AddMovementInput(Vector3 WorldDirectionMove)
        {
            d_CharacterMove = WorldDirectionMove;
            d_CharacterMove = Vector3.ClampMagnitude(d_CharacterMove, movementSettings.Speed);
        }
        IEnumerator GroundMoveDelay(float time)
        {
            yield return new WaitForSeconds(time);
            GroundMove();
        }

        public void TakeDamage(int damage)
        {
            m_Health = Mathf.Clamp(m_Health - damage, 0, 100);
			Debug.Log ("player hp: " + m_Health);
            if (m_Health == 0)
            {
                Death();
            }
        }
        public void setHealth(int health)
        {
            m_Health = health;
        }

        private void Death()
        {
            //Pause the game
            m_Recording = false;
            m_Dead = true;
            TimeControl.SetPaused(true);
            TimeControl.ResetTime();//Go Back to the beginning of the time loop
            //Check to see if any characters are in limbo, if so, "exit limbo" for those characters
            if (TimeControl.limboCharacters != null && TimeControl.limboCharacters.Count > 0)
            {
                TimeControl.limboCharacters.Pop().ExitLimbo(this);
                return;
            }
            //If no characters are in limbo...
            //If it's the final spawn point Game Over
            var nextPoint = FindObjectOfType<SpawnPointManager>().ConsumeSpawnPoint();
            if (nextPoint == null)
            {
                //Game Over
                return;
            }

            CurrentSpawnPoint = NextSpawnPoint;
            // Then update the new spawn point
            NextSpawnPoint = new GameObject().AddComponent<SpawnPoint>();
            NextSpawnPoint.transform.position = nextPoint.position;


            //Create an AI Character that will act out the players previous actions, and place them at the current spawn point

            PlayerReplay newReplayCharacter = Instantiate(ReplayCharacter, NextSpawnPoint.GetPosition(), NextSpawnPoint.GetRotation());
            newReplayCharacter.m_RecordingLog = m_RecordingLog;
            //Spawn a player controlled character at the next spawn point
            RigidbodyFirstPersonController newPlayer = Instantiate(this, NextSpawnPoint.GetPosition(), NextSpawnPoint.GetRotation());
            //Set the next spawn point as the current spawn point
            newPlayer.CurrentSpawnPoint = CurrentSpawnPoint;
            //Set the next spawn point's "Next spawn point" as the next spawn point
            newPlayer.NextSpawnPoint = NextSpawnPoint.NextSpawnPoint;
            //Change Control to this new Character
            newPlayer.cam.gameObject.SetActive(true);
            cam.gameObject.SetActive(false);
            //Set PlayerReplay reference to current character
            newReplayCharacter.SetCurrentPlayer(newPlayer);
            List<PlayerReplay> ReplayCharacters = new List<PlayerReplay>(FindObjectsOfType<PlayerReplay>());
            foreach (PlayerReplay ReplayCharacter in ReplayCharacters)
            {
                // start back from the beginning
                ReplayCharacter.SetCurrentPlayer(newPlayer);
                ReplayCharacter.m_CommandLog = new Queue<recordCommand>(ReplayCharacter.m_RecordingLog);
            }
            //Delete old Character
            Destroy(gameObject);
            //Wait for player input to unpause the game
            //This is in Update()
        }

        public void EnterLimbo(PlayerReplay callingCharacter)
        {
            // Pause the Game
            m_Recording = false;
            m_Dead = true;
            TimeControl.SetPaused(true);
            // Placeholder in-limbo character is created
            PlayerLimbo newLimboPlayer = Instantiate(LimboCharacter, this.transform.position, Quaternion.Euler(0.0f, cam.transform.rotation.eulerAngles.y, 0.0f));
            // Set the variabales in the Limbo Character
            newLimboPlayer.m_Health = m_Health;
            newLimboPlayer.m_RecordingLog = m_RecordingLog;
            newLimboPlayer.NextSpawnPoint = NextSpawnPoint;
            newLimboPlayer.CurrentSpawnPoint = CurrentSpawnPoint;
            newLimboPlayer.m_EnterLimboTime = TimeControl.GetTimeRemaining();
            // The character in limbo is pushed into a stack
            TimeControl.limboCharacters.Push(newLimboPlayer);

            // If the player is entering limbo because another character has reached the end of it's recorded actions but time is not yet up...
            if (TimeControl.GetTimeRemaining() > 0)
            {
                // A player character is spawned at the point where this old character is
                RigidbodyFirstPersonController newPlayer = Instantiate(this, callingCharacter.transform.position, callingCharacter.transform.rotation);
                //Set the new players stats
                newPlayer.m_RecordingLog = callingCharacter.m_RecordingLog;
                newPlayer.m_Health = callingCharacter.GetHealth();
                newPlayer.cam.transform.rotation = callingCharacter.transform.rotation;
                //Change Control to this new Character
                newPlayer.cam.gameObject.SetActive(true);
                cam.gameObject.SetActive(false);
                //Delete the calling character
                Destroy(callingCharacter.gameObject);
                //Delete old Character
                Destroy(gameObject);
                // Game waits for the player to un-pause it
            }
        }

        private void Crouch(float DeltaTime)
        {
            if (m_Crouched == false && d_GoalHeight != d_StandingHeight && !TimeControl.GetPaused())
            {
                RaycastHit hitInfo;
                if (!(Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.up, out hitInfo,
                   ((d_StandingHeight / 2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore)))
                {
                    d_StartCrouchHeight = m_Capsule.height;
                    d_StartPosition = m_RigidBody.transform.position.y;
                    d_GoalHeight = d_StandingHeight;
                    d_StartCameraHeight = cam.transform.localPosition.y;
                    d_CameraGoalHeight = d_CameraStandingHeight;
                }
            }
            if (m_Capsule.height != d_GoalHeight)
            {
                CrouchTime += DeltaTime * 10;
                float CurrentHeight = Mathf.Lerp(d_StartCrouchHeight, d_GoalHeight, CrouchTime);
                float CameraHeight = Mathf.Lerp(d_StartCameraHeight, d_CameraGoalHeight, CrouchTime);
                Vector3 newPosition = new Vector3(
                        m_RigidBody.transform.position.x,
                        m_RigidBody.transform.position.y - ((CurrentHeight - d_GoalHeight) / 2),
                        m_RigidBody.transform.position.z);
                if (d_StartCrouchHeight < d_GoalHeight && newPosition.y > d_StartPosition - ((d_StartCrouchHeight - d_GoalHeight) / 2))
                {
                    newPosition.y = d_StartPosition - ((d_StartCrouchHeight - d_GoalHeight) / 2);
                }
                if (d_StartCrouchHeight > d_GoalHeight && newPosition.y < d_StartPosition - ((d_StartCrouchHeight - d_GoalHeight) / 2))
                {
                    //newPosition.y = d_StartPosition - ((d_GoalHeight - d_StartCrouchHeight) / 2);
                }
                m_RigidBody.transform.position = newPosition;
                m_Capsule.height = Mathf.Round(CurrentHeight * 100f) / 100f;
                cam.transform.localPosition = new Vector3(0.0f, d_CameraStandingHeight, 0.0f);
                cam.transform.localPosition = new Vector3(0.0f, CameraHeight, 0.0f);
                m_Crouching = true;
            }
            else
            {
                CrouchTime = 0;
                m_Crouching = false;
            }
        }

        private void Start()
        {
            m_RigidBody = GetComponent<Rigidbody>();
			m_Capsule = GetComponent<CapsuleCollider>();
            mouseLook.Init (transform, cam.transform);
            m_Health = m_MaxHealth;

            //Record Default Values
            d_DefaultMaxWalkSpeed = movementSettings.Speed;
            d_DefaultBraking = movementSettings.Deceleration;
            d_StandingHeight = 1.8f;
            d_CameraStandingHeight = 0.6f;

            m_RecordingLog = new List<recordCommand>();

            var startPos = FindObjectOfType<SpawnPointManager>().ConsumeSpawnPoint();
            var start = new GameObject().AddComponent<SpawnPoint>();
            start.transform.position = startPos.position;

            gameObject.transform.position = startPos.position;

            CurrentSpawnPoint = start;

        }
        private void Update()
        {
            if (CrossPlatformInputManager.GetButtonDown("Cancel"))
            {
                TimeControl.SetPaused(!TimeControl.GetPaused());
            }
            if (TimeControl.GetPaused())
                return;
            RotateView();
            mouseLook.UpdateCursorLock();
            Vector2 input = GetInput();

            if (CrossPlatformInputManager.GetButtonDown("Jump") && !m_Jump)
            {
                m_Jump = true;
            }

            // Crouching
            if (CrossPlatformInputManager.GetButtonDown("Crouch"))
            {
                m_Crouched = true;
                d_StartCrouchHeight = m_Capsule.height;
                d_StartPosition = m_RigidBody.transform.position.y;
                d_GoalHeight = movementSettings.CrouchHeight;
                d_StartCameraHeight = cam.transform.localPosition.y;
                d_CameraGoalHeight = movementSettings.CameraCrouchHeight;
            }
            if (CrossPlatformInputManager.GetButtonUp("Crouch"))
            {
                m_Crouched = false;
                RaycastHit hitInfo;
                if (!(Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.up, out hitInfo,
                   ((d_StandingHeight / 2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore)))
                {
                    d_StartCrouchHeight = m_Capsule.height;
                    d_StartPosition = m_RigidBody.transform.position.y;
                    d_GoalHeight = d_StandingHeight;
                    d_StartCameraHeight = cam.transform.localPosition.y;
                    d_CameraGoalHeight = d_CameraStandingHeight;
                }
            }

            if (CrossPlatformInputManager.GetButtonDown("EndTurn") && !m_Dead)
            {
                Death();
            }

            d_PrevVel.x = m_RigidBody.velocity.x;
            d_PrevVel.z = m_RigidBody.velocity.z;
            d_PrevVel.y = 0.0f;
            d_XZSpeedometer = d_PrevVel.magnitude;
            d_YVelocity = m_RigidBody.velocity.y;

            d_FrameTime = Time.deltaTime;

            d_InAir = !m_IsGrounded;
            if (d_InAir)
            {
                RemoveFriction();
            }

            //Move Right
            if (Mathf.Abs(input.x) > float.Epsilon)
            {
                MoveRight(input.x);
                advancedSettings.airControl = true;
            }
            else
            {
                MoveRight(0.0f);
                advancedSettings.airControl = false;
            }
            //Move Forward
            if (Mathf.Abs(input.y) > float.Epsilon)
            {
                MoveForward(input.y);
                advancedSettings.airControl = true;
            }
            else
            {
                MoveForward(0.0f);
                advancedSettings.airControl = false;
            }
            // Apply Movement
            if (Mathf.Abs(input.x) > float.Epsilon || Mathf.Abs(input.y) > float.Epsilon)
            {
                // always move along the camera forward as it is the direction that it being aimed at
                Vector3 desiredMove = d_CharacterMove;
                desiredMove = Vector3.ProjectOnPlane(desiredMove, m_GroundContactNormal).normalized;

                desiredMove = desiredMove * movementSettings.CurrentTargetSpeed;
                if (m_RigidBody.velocity.sqrMagnitude <
                    (movementSettings.CurrentTargetSpeed * movementSettings.CurrentTargetSpeed))
                {
                    m_RigidBody.AddForce(desiredMove * SlopeMultiplier() * movementSettings.Acceleration, ForceMode.Impulse);
                }
                m_SmoothingVelocity = Vector3.zero;
                m_StopTime = d_XZSpeedometer / 100;
            }
            else
            {
                AddMovementInput(Vector3.zero);
                if (!d_InAir)
                {
                    m_RigidBody.velocity = Vector3.SmoothDamp(m_RigidBody.velocity, new Vector3(0.0f, m_RigidBody.velocity.y, 0.0f), ref m_SmoothingVelocity, m_StopTime);
                }
            }

            // Set the speed variables and time scale variables
            m_HorizontalSpeed.x = m_RigidBody.velocity.x;
            m_HorizontalSpeed.z = m_RigidBody.velocity.z;
            if (advancedSettings.VerticalMoveTimeScale)
            {
                m_HorizontalSpeed.y = m_RigidBody.velocity.y;
            }
            timeScaleMove = Mathf.Clamp(m_HorizontalSpeed.magnitude / movementSettings.CurrentTargetSpeed, 0, 1);
            timeScaleMove = (timeScaleMove < 0.001 ? 0.0f : timeScaleMove);
            if (m_Crouching)
            {
                timeScaleMove = 1;
            }
            m_RotationChange = Quaternion.Angle(m_CameraRotation, cam.transform.rotation) * Time.deltaTime * 100;
            timeScaleLook = Mathf.Clamp(m_RotationChange, 0, 1);
            timeScaleLook = (timeScaleLook < 0.001 ? 0.0f : timeScaleLook);
            m_CameraRotation = cam.transform.rotation;

            TimeControl.TimeSlow(timeScaleLook + timeScaleMove);
        }

        public void Fire()
        {
            m_Firing = true;
            if (m_Recording)
            {
                AddToRecordingLog(true);
            }
        }

        private void FixedUpdate()
        {
            GroundCheck();
            Vector2 input = GetInput();

            Crouch(Time.fixedDeltaTime);

            if (m_IsGrounded)
            {
                m_RigidBody.drag = 5f;

                if (m_Jump)
                {
                    m_RigidBody.drag = 0f;
                    m_RigidBody.velocity = new Vector3(m_RigidBody.velocity.x, 0f, m_RigidBody.velocity.z);
                    m_RigidBody.AddForce(new Vector3(0f, movementSettings.JumpForce, 0f), ForceMode.Impulse);
                    m_Jumping = true;
                }

                if (!m_Jumping && Mathf.Abs(input.x) < float.Epsilon && Mathf.Abs(input.y) < float.Epsilon && m_RigidBody.velocity.magnitude < 1f)
                {
                    m_RigidBody.Sleep();
                }
            }
            else
            {
                m_RigidBody.drag = 0f;
                if (m_PreviouslyGrounded && !m_Jumping)
                {
                    StickToGroundHelper();
                }
            }
            m_Jump = false;

			if (m_Firing)
			{
                m_Firing = false;
            }

            // Set the speed variables and time scale variables
            m_HorizontalSpeed.x = m_RigidBody.velocity.x;
            m_HorizontalSpeed.z = m_RigidBody.velocity.z;
            timeScaleMove = Mathf.Clamp(m_HorizontalSpeed.magnitude / movementSettings.CurrentTargetSpeed, 0, 1);
            m_RotationChange = Quaternion.Angle(m_CameraRotation, cam.transform.rotation) * Time.deltaTime * 100;
            timeScaleLook = Mathf.Clamp(m_RotationChange, 0, 1);
            m_CameraRotation = cam.transform.rotation;

            timeRemaining = TimeControl.GetTimeRemaining();
            // Debug.Log("Time Remaining: " + timeRemaining);
            // When you run out of time, you're turn ends and it's onto the next character
            m_TimeSinceLastTick += Time.fixedDeltaTime;
            if (m_Recording && m_TimeSinceLastTick >= RECORDING_TICK)
            {
                AddToRecordingLog(false);
                m_TimeSinceLastTick = 0.0f;
            }
        }

        private void AddToRecordingLog(bool fire)
        {
            recordCommand command = new recordCommand();
            command.fire = fire;
            command.position = m_RigidBody.transform.position;
            command.rotation = cam.transform.rotation;
            command.velocity = m_RigidBody.velocity;
            command.crouchHeight = m_Capsule.height;
            command.timeRemaining = TimeControl.GetTimeRemaining();
            m_RecordingLog.Add(command);
        }

        private float SlopeMultiplier()
        {
            float angle = Vector3.Angle(m_GroundContactNormal, Vector3.up);
            return movementSettings.SlopeCurveModifier.Evaluate(angle);
        }


        private void StickToGroundHelper()
        {
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((m_Capsule.height/2f) - m_Capsule.radius) +
                                   advancedSettings.stickToGroundHelperDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                if (Mathf.Abs(Vector3.Angle(hitInfo.normal, Vector3.up)) < 85f)
                {
                    m_RigidBody.velocity = Vector3.ProjectOnPlane(m_RigidBody.velocity, hitInfo.normal);
                }
            }
        }


        private Vector2 GetInput()
        {
            
            Vector2 input = new Vector2
                {
                    x = CrossPlatformInputManager.GetAxis("Horizontal"),
                    y = CrossPlatformInputManager.GetAxis("Vertical")
                };
			movementSettings.UpdateDesiredTargetSpeed(input);
            return input;
        }


        private void RotateView()
        {
            //avoids the mouse looking if the game is effectively paused
            if (Mathf.Abs(Time.timeScale) < float.Epsilon) return;

            // get the rotation before it's changed
            float oldYRotation = transform.eulerAngles.y;

            mouseLook.LookRotation (transform, cam.transform);

            if (m_IsGrounded || advancedSettings.airControl)
            {
                // Rotate the rigidbody velocity to match the new direction that the character is looking
                Quaternion velRotation = Quaternion.AngleAxis(transform.eulerAngles.y - oldYRotation, Vector3.up);
                m_RigidBody.velocity = velRotation*m_RigidBody.velocity;
            }
        }

        /// sphere cast down just beyond the bottom of the capsule to see if the capsule is colliding round the bottom
        private void GroundCheck()
        {
            m_PreviouslyGrounded = m_IsGrounded;
            RaycastHit hitInfo;
            if (Physics.SphereCast(transform.position, m_Capsule.radius * (1.0f - advancedSettings.shellOffset), Vector3.down, out hitInfo,
                                   ((m_Capsule.height/2f) - m_Capsule.radius) + advancedSettings.groundCheckDistance, Physics.AllLayers, QueryTriggerInteraction.Ignore))
            {
                m_IsGrounded = true;
                m_GroundContactNormal = hitInfo.normal;
            }
            else
            {
                m_IsGrounded = false;
                m_GroundContactNormal = Vector3.up;
            }
            if (!m_PreviouslyGrounded && m_IsGrounded && m_Jumping)
            {
                m_Jumping = false;
            }
        }
    }

   
}
