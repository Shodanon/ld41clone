﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public class DynamicCommand : Command
    {

        public virtual Command FreezeState()
        {
            return new Log("This is a smart command! " + Time.frameCount);
        }
    }


    public class CreatePathCommand : DynamicCommand
    {
        private float m_speed;
        private Vector3 m_goal;
        public CreatePathCommand(Vector3 goal, float speed)
        {
            m_speed = speed;
            m_goal = goal;
        }
        public override Command FreezeState()
        {
            var agent = m_target.GetComponent<NavMeshAgent>();
            NavMeshPath path = new NavMeshPath();
            NavMeshHit hit = new NavMeshHit();
            // Path length is 0 for some reason 

            NavMesh.SamplePosition(m_goal, out hit, 5f, NavMesh.AllAreas);
            agent.CalculatePath(hit.position, path);
            Debug.Log(path.corners.Length);
            return new Move(path, m_speed);
        }
    }
}
