﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;

namespace AI
{
	[RequireComponent(typeof(BoxCollider))]
	public class Projectile : MonoBehaviour
	{
		public Vector3 m_velocity;
		public List<GameObject> m_collisionExceptions;
        public GameObject HitEffect;
		public GameObject BloodEffect;
		public float m_damage = 1.0F;

		public Projectile (Vector3 position, Vector3 velocity, float damage)
		{
			transform.position = position;
			m_velocity = velocity;
			m_collisionExceptions = new List<GameObject> ();
		}

		public Projectile (Vector3 position, Vector3 velocity, float damage, List<GameObject> collisionExceptions)
		{
			transform.position = position;
			m_velocity = velocity;
			m_collisionExceptions = collisionExceptions;
		}

		void Start ()
		{
			if (m_collisionExceptions == null)
				m_collisionExceptions = new List<GameObject> ();
		}
		
		void FixedUpdate()
		{
            if (TimeControl.GetPaused())
                return;
			transform.position = transform.position + m_velocity * Time.deltaTime * Time.timeScale;
			transform.LookAt (transform.position + m_velocity * 5);
		}

		void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == "Projectile")
				return;
			if (other.gameObject.GetComponent<WeaponProperties> ())
				return;
			
			bool damageOther = true;
			foreach (GameObject g in m_collisionExceptions) {
				if (g.Equals (other.gameObject)) {
					damageOther = false;
					return;
				}
			}

			if (damageOther) {
				if (other.gameObject.tag == "Enemy") {
					if (other is CapsuleCollider)
						return;
					other.gameObject.GetComponent<EnemyUnit> ().Damage (m_damage);
					//Vector3 bloodRot = (other.transform.position - transform.position).normalized;
					GameObject.Instantiate (BloodEffect, transform.position, transform.rotation).transform.parent = other.transform;
					//GameObject.Instantiate (BloodEffect, transform.position, Quaternion.Euler(bloodRot)).transform.parent = other.transform;
				} else if (other.gameObject.GetComponent<RigidbodyFirstPersonController> ())
				{
					other.gameObject.GetComponent<RigidbodyFirstPersonController> ().TakeDamage ((int)m_damage);
					GameObject.Instantiate (BloodEffect, transform.position, other.transform.rotation).transform.parent = other.transform;
				}
				else
					GameObject.Instantiate(HitEffect, transform.position, transform.rotation);
			}

			Destroy (this.gameObject);
		}

		void addCollisionException(GameObject gameObject)
		{
			m_collisionExceptions.Add (gameObject);
		}
	}

	public class SpawnProjectile : Command
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AI.SpawnProjectile"/> class.
		/// </summary>
		/// <param name="position">Initial position of the projectile.</param>
		/// <param name="velocity">Initial velocity of the projectile.</param>
		/// <param name="collisionExceptions">A List of GameObjects that the Projectile should not collide with.</param>
		public SpawnProjectile(Vector3 position, Vector3 velocity, float damage, List<GameObject> collisionExceptions, GameObject bulletPrefab)
		{
            // this is very hacky but gets the job done
			bulletPrefab.transform.position = position;
			bulletPrefab.GetComponent<Projectile>().m_velocity = velocity;
			bulletPrefab.GetComponent<Projectile>().m_damage = damage;
            bulletPrefab.GetComponent<Projectile>().m_collisionExceptions = collisionExceptions;
            bulletPrefab.transform.LookAt (position + velocity);
		}
			
		public override System.Collections.IEnumerator Process()
		{
			yield return null;
		}
	}
}

