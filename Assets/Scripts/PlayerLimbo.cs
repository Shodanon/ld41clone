﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerLimbo : MonoBehaviour {

    public RigidbodyFirstPersonController PlayerCharacter;

    public List<recordCommand> m_RecordingLog;
    public int m_Health;
    public SpawnPoint CurrentSpawnPoint;
    public SpawnPoint NextSpawnPoint;
    public float m_EnterLimboTime;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExitLimbo(RigidbodyFirstPersonController callingCharacter)
    {
        // Time Rewinds to when the player entered limbo
        TimeControl.SetTimeRemaining(m_EnterLimboTime);
        List<PlayerReplay> ReplayCharacters = new List<PlayerReplay>(FindObjectsOfType<PlayerReplay>());
        foreach (PlayerReplay ReplayCharacter in ReplayCharacters)
        {
            // start back from the beginning
            ReplayCharacter.m_CommandLog = new Queue<recordCommand>(ReplayCharacter.m_RecordingLog);
        }
        // Player character is spawend at the point where the in-limbo character is
        RigidbodyFirstPersonController newPlayer = Instantiate(PlayerCharacter, callingCharacter.transform.position, callingCharacter.transform.rotation);
        //Set the new players stats
        newPlayer.m_RecordingLog = callingCharacter.m_RecordingLog;
        newPlayer.setHealth(m_Health);
        newPlayer.cam.transform.rotation = callingCharacter.transform.rotation;
        newPlayer.CurrentSpawnPoint = CurrentSpawnPoint;
        newPlayer.NextSpawnPoint = NextSpawnPoint;
        //Change Control to this new Character
        newPlayer.cam.gameObject.SetActive(true);
        callingCharacter.cam.gameObject.SetActive(false);
        //Delete the calling character
        Destroy(callingCharacter);
        //Delete the limbo character Character
        Destroy(gameObject);
    }
}
