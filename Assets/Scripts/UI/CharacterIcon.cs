﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterIcon : MonoBehaviour
{
    private Image deathIcon;
    private Image characterFill;
    private Image characterLock;
    private Image characterPlay;
    private Image charIcon;
    private Color[] HealthColors;


    //  public int hp;
    //  public float time;
    //  public bool play;
    //   public bool pause;

    // Use this for initialization
    void Start()
    {
        HealthColors = new Color[] { Color.black, Color.red, Color.yellow, Color.green, Color.white };
        deathIcon = transform.Find("CharacterDead").GetComponent<Image>();
        characterFill = transform.Find("CharTimer").GetComponent<Image>();
        characterLock = transform.Find("Lock").GetComponent<Image>();
        charIcon = transform.Find("CharacterIcon").GetComponent<Image>();
        characterPlay = transform.Find("Play").GetComponent<Image>();
        deathIcon.enabled = false;
        characterLock.enabled = false;
        characterPlay.enabled = false;
        DisplayHealthValue(101);

    }


    //update is debug stuff
    //private void Update()
    // {
    //     if (play)
    //     {
    //         characterLock.enabled = false;
    //          characterPlay.enabled = true;

    //      }
    //      if (pause)
    //      {
    //          characterLock.enabled = true;
    //          characterPlay.enabled = false;

    //     }
    //      DisplayHealthValue(hp);
    //     UpdateTime(time, 15);
    //  }

    void DisplayHealthValue(int HealthValue)
    {

        if (HealthValue < 1)
        {
            deathIcon.enabled = true;
            charIcon.color = HealthColors[0];
        }
        else
        {
            if (HealthValue > 100)
            {
                charIcon.color = HealthColors[4];
            }
            else
            {
                charIcon.color = Color.Lerp(HealthColors[HealthValue / 33], HealthColors[HealthValue / 33 + 1], ((float)HealthValue / 33f) % 1f);
            }
        }



    }


    void UpdateTime(float CurrentTime, float Maxtime)
    {
        if (CurrentTime > 0)
        {
            characterFill.fillAmount = CurrentTime / Maxtime;
        }
        else
        {
            characterFill.fillAmount = 0;
            characterLock.enabled = true;
        }
    }
}
