﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDisplay : MonoBehaviour
{

    private Text TimeDisplay;
    private Image TimeWheel;
     public float timer = 0;
    void Start()
    {
        TimeDisplay = transform.Find("Display").GetComponent<Text>();
        TimeWheel = transform.Find("Fill").GetComponent<Image>();
        TimeWheel.fillAmount = 1;
    }

      void FixedUpdate()
      {
          timer += Time.fixedDeltaTime;
          UpdateTime(timer, 15);
     }

    void UpdateTime(float CurrentTime, float Maxtime)
    {
        float timeFraction = 0;
        string seconds = "00";
        int properTimeFraction = 0;
        string fractionDisplay = ":00";
        if (CurrentTime > 0)
        {

            timeFraction = CurrentTime % 1f;
            properTimeFraction = (int)(((timeFraction * 10f) - (timeFraction * 10f) % 1f) * 6);
            if (properTimeFraction < 10)
            {
                fractionDisplay = ":0" + properTimeFraction;
            }
            else
            {
                fractionDisplay = ":" + properTimeFraction;
            }

            if (CurrentTime - timeFraction > 9)
            {
                seconds = "" + (CurrentTime - timeFraction);
            }
            else
            {
                seconds = "0" + (CurrentTime - timeFraction);
            }
        }
        else
        {
            CurrentTime = 0;

        }

        TimeDisplay.text = seconds + fractionDisplay;
      //  TimeWheel.fillAmount = CurrentTime / Maxtime;

    }
}