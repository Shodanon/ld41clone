﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class HealthDisplay : MonoBehaviour
{
    private Text healthPegs;
    private Text healthPercentage;
    private string pegs;
    private RigidbodyFirstPersonController rbcontroller;
    // public int val;

    void Start()
    {
        rbcontroller = GameObject.Find("RigidBodyFPSController").GetComponent<RigidbodyFirstPersonController>();
        healthPegs = transform.Find("HealthPegs").GetComponent<Text>();
        healthPercentage = transform.Find("HealthAmount").GetComponent<Text>();

    }

    //Using this for debug
    private void Update()
    {


        DisplayHealthValue(rbcontroller.m_Health);
     }

    void DisplayHealthValue(int HealthValue)
    {
        pegs = "";
        if (HealthValue > 0)
        {
            pegs = "|";

            for (int Pegno = 0; Pegno < HealthValue / 5 - 1; ++Pegno)
            {
                pegs = pegs + "|";
            }
        }
        else
        {
            HealthValue = 0;
        }
        healthPegs.text = pegs;
        healthPercentage.text = HealthValue + "%";
    }
}
