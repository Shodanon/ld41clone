﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerReplay : MonoBehaviour {

    private const float RECORDING_TICK = TimeControl.RECORDING_TICK;

    public List<recordCommand> m_RecordingLog;
    public Queue<recordCommand> m_CommandLog;
    private recordCommand m_CurrentCommand;

    private RigidbodyFirstPersonController m_player;
    private Rigidbody m_RigidBody;
    private CapsuleCollider m_Capsule;
    private int m_Health;
    private float m_LastTick = 30.0f;
    private bool m_Firing = false;
    private Vector3 m_GoalPosition = Vector3.zero;
    private Vector3 m_GoalVelocity = Vector3.zero;
    private Quaternion m_GoalAngle = Quaternion.identity;

    private Animator AttachedAnimator;

    public void TakeDamage(int damage)
    {
        m_Health = Mathf.Clamp(m_Health - damage, 0, 100);
        if (m_Health == 0)
        {
            Death();
        }
    }

    private void Death()
    {
        m_RigidBody.useGravity = true;
    }

    public int GetHealth()
    {
        return m_Health;
    }

    // Use this for initialization
    void Start ()
    {
        AttachedAnimator = transform.GetComponent<Animator>();
        m_RigidBody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();
        m_CurrentCommand.timeRemaining = 30.0f;
        m_CommandLog = new Queue<recordCommand>(m_RecordingLog);
    }

    // Update is called once per frame
    void Update ()
    {
        if (m_LastTick - TimeControl.GetTimeRemaining() >= RECORDING_TICK && !TimeControl.GetPaused())
        {
            if (m_CommandLog.Count <= 0 && m_player != null && TimeControl.GetTimeRemaining() > 0)
            {
                //When running out of commands.
                m_player.EnterLimbo(this);
                m_RigidBody.isKinematic = true;
                m_RigidBody.velocity = Vector3.zero;
                TimeControl.SetPaused(true);
                return;
            }
            while (m_CurrentCommand.timeRemaining >= TimeControl.GetTimeRemaining())
            {
                if (m_CommandLog.Count <= 0)
                    break;
                m_CurrentCommand = m_CommandLog.Dequeue();
                m_RigidBody.transform.position = m_CurrentCommand.position;
                m_RigidBody.velocity = m_CurrentCommand.velocity;
                m_RigidBody.rotation = Quaternion.Euler(0.0f, m_CurrentCommand.rotation.eulerAngles.y, 0.0f);
                m_Capsule.height = m_CurrentCommand.crouchHeight;

                
                if (m_CurrentCommand.fire)
                {
                    m_Firing = true;
                }
                else
                {
                    m_Firing = false;
                }
                if (m_CommandLog.Count <= 0)
                    break;
            }
            m_LastTick = TimeControl.GetTimeRemaining();
        }
    }

    public void SetCurrentPlayer(RigidbodyFirstPersonController newPlayer)
    {
        m_player = newPlayer;
    }

    private void FixedUpdate()
    {
        
    }
}
