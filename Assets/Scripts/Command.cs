﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AI
{
    public enum Name
    {
        Attack,
        Move,
        Crouch
    }

    public class Command
    {
        protected IEnumerator m_process = null;
        protected GameObject m_target = null;
        protected float m_duration = float.MaxValue; //Just set it absurdly high because we're pajeets
        protected float m_runtime = 0;
        public IEnumerator Fire(GameObject gameObject) {
            Debug.Assert(m_process == null, "You can't execute the same command twice simultaneously");
            m_target = gameObject;
            m_runtime = 0;
            yield return Process();
        }
        public virtual void Cancel() {
            m_duration = m_runtime;
        }

        // Pretend the command finished, since we're restarting the level and will want it later
        public virtual void Restarting()
        {

        }

        public virtual IEnumerator Process()
        {
            yield return null;
        }

        public void NotifyTimePassed(float seconds)
        {
            m_runtime += seconds;
        }
    }

    public class Attack : Command
    {
        public Attack(Vector3 target)
        {

        }
    }

    public class Move : Command
    {
        private Vector3 m_goal;
		private float m_speed;
		private NavMeshAgent m_agent;
        private NavMeshPath m_precalculated_path;
		public Move(Vector3 pos, float speed)
        {
			m_goal = pos;
			m_speed = speed;
        }

        public Move(NavMeshPath precalculated_path, float speed)
        {
            m_speed = speed;
            m_precalculated_path = precalculated_path;
        }

        public override void Restarting()
        {
            
        }

        public override IEnumerator Process()
     	{
            m_agent = m_target.GetComponent<NavMeshAgent>();
            Debug.Assert(m_agent != null, "Object must have navmesh agent to Move.");
            m_agent.speed = m_speed;

            if (m_precalculated_path == null)
            {
                if (!m_agent.SetDestination(m_goal))
                Debug.LogWarning("Pathfinding skipped, invalid position : " + m_goal);
            } else
            {
                m_agent.SetPath(m_precalculated_path);
                // m_agent.Warp(m_precalculated_path.corners[0]);
            }
            // Let unity decide its new path
            yield return null;

            while ((m_runtime + float.Epsilon) < m_duration) 
            {
                // Takes a lot of work to see if we're done all we can to get to a path
                if (m_agent.remainingDistance <= m_agent.stoppingDistance && (!m_agent.hasPath || m_agent.velocity.sqrMagnitude < float.Epsilon))
                {
                    m_runtime = m_duration;
                }
                yield return new WaitForEndOfFrame();
            }
         }
    }

	public class Stop : Command
	{
		private NavMeshAgent m_agent;
		public Stop()
		{
		}
		public override IEnumerator Process ()
		{
			m_agent = m_target.GetComponent<NavMeshAgent>();
			Debug.Assert(m_agent != null, "Object must have navmesh agent to Stop.");
			m_agent.SetDestination (m_target.transform.position);
			return base.Process ();
		}
	}

    public class Log : Command
    {
        private string str;
        public Log(string str)
        {
            this.str = str;
        }
        public override IEnumerator Process()
        {
            Debug.Log("Command Debug : " + str);
            yield return null;
        }
    }
}
