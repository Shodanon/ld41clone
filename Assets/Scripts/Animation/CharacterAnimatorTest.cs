﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CharacterAnimatorTest : MonoBehaviour
{
    private Animator AttachedAnimator;

    private bool HasRigidBody;


    public Vector3 VelocityVector;
    public float YRotation;
    private NavMeshAgent agent;
    private Rigidbody rigiBody;
    private float MovementSpeed;
    private Vector3 InitialVelocityVector;
    // Use this for initialization
    void Start()
    {
        AttachedAnimator = transform.GetComponent<Animator>();
        if (transform.GetComponent<Rigidbody>() != null)
        {
            HasRigidBody = true;
            rigiBody = transform.GetComponent<Rigidbody>();
            AttachedAnimator.SetFloat("LookUpDown", 0.5f);
        }
        else
        {
            agent = transform.GetComponent<NavMeshAgent>();

        }
    }

    // Update is called once per frame
    void Update()
    {
        YRotation = transform.eulerAngles.y;
        if (HasRigidBody)
        {
            InitialVelocityVector = rigiBody.velocity.normalized;
            MovementSpeed = rigiBody.velocity.magnitude;
        }
        else
        {
            InitialVelocityVector = agent.velocity.normalized;
            MovementSpeed = agent.velocity.normalized.magnitude;
        }

    }

    private void FixedUpdate()
    {
        AttachedAnimator.SetFloat("LookUpDown", 0.5f);

        VelocityVector = Quaternion.Euler(new Vector3(0, -YRotation, 0)) * InitialVelocityVector;

        if (MovementSpeed > 0.05f)
        {
            AttachedAnimator.SetFloat("RunSpeed", MovementSpeed);
        }
        else
        {
            //AttachedAnimator.SetTrigger("StopRun");

        }
        Vector3 ModifiedVelocity = Vector3.zero;
        float vectorValue = 0.1f + (Mathf.Rad2Deg * Mathf.Atan(Mathf.Abs(VelocityVector.z) / Mathf.Abs(VelocityVector.x))) / 90f;

        
            AttachedAnimator.SetFloat("NS", VelocityVector.z);
        
            AttachedAnimator.SetFloat("EW", VelocityVector.x);
        AttachedAnimator.SetFloat("VectorDiff", (Mathf.Rad2Deg* Mathf.Atan (Mathf.Abs( VelocityVector.x)/ Mathf.Abs(VelocityVector.z)))/90f);


    }
}
